<?php
/*
    ./noyau/constantes.php
    Constantes du framework
 */

  $local_path = str_replace(basename($_SERVER['SCRIPT_NAME']) , '', $_SERVER['SCRIPT_NAME']);

  define('ROOT_PUBLIC', 'http://'
                . $_SERVER['HTTP_HOST']
                . $local_path);

  define('ROOT_BACKOFFICE', str_replace(FOLDER_PUBLIC, FOLDER_BACKOFFICE, ROOT_PUBLIC));
